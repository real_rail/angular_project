import {Component} from "@angular/core";
import {UserService} from "../../../services/user.service";
import {Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'auth-form',
    templateUrl: 'auth-form.component.html',
    styleUrls: ['auth-form.component.css']
})
export class AuthFormComponent {
    login: string = '';
    password: string = '';
    token: string;

    constructor(private userService: UserService, private router: Router) {}

    onSubmit() {
        this.userService.authentication(this.login, this.password).then(token => this.afterAuth(token));
    }

    afterAuth(token: string) {
        this.token = token;
        this.userService.setToken(token);
        if (this.userService.isTokenValid(this.userService.getToken()))
            this.router.navigate(['/catalog']);
        else alert('Неправильные данные');
    }
}