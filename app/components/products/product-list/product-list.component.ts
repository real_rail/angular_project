import {Component, OnInit} from "@angular/core";
import {Product} from "../../../models/product";
import {ProductService} from "../../../services/product.service";

@Component({
    moduleId: module.id,
    selector: 'product-list',
    templateUrl: 'product-list.component.html',
    styleUrls: ['product-list.component.css']
})
export class ProductListComponent implements OnInit {
    products: Product[];

    constructor(private productService: ProductService) {
        this.products = [];
    }

    ngOnInit() {
        this.productService.getProducts().then(products => this.products = products);
    }
}