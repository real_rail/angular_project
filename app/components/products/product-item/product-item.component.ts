import {Component, EventEmitter, Input, Output} from "@angular/core";
import {Product} from "../../../models/product";
import {UserService} from "../../../services/user.service";
import {BasketService} from "../../../services/basket.service";
import {Router} from "@angular/router";

@Component({
    moduleId: module.id,
    selector: 'product-item',
    templateUrl: 'product-item.component.html',
    styleUrls: ['product-item.component.css']
})
export class ProductItemComponent {
    @Input() product: Product;

    addableToCart: boolean = true;

    constructor(private userService: UserService, private basketService: BasketService, private router: Router){}

    ngOnInit(){
        this.addableToCart = this.userService.isTokenValid(this.userService.getToken());
        this.userService.tokenUpdated.subscribe(
            (token: string) => {
                this.addableToCart = this.userService.isTokenValid(token);
            }
        );
    }

    onAddingToCart(){
        if (this.basketService.addToCart(this.product.id))
            this.router.navigate((['/basket']));
    }
}