import {Component, OnInit} from "@angular/core";
import {Product} from "../../../models/product";
import {ProductService} from "../../../services/product.service";
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs/Subscription";

@Component({
    moduleId: module.id,
    selector: 'product-page',
    templateUrl: 'product-page.component.html',
    styleUrls: ['product-page.component.css']
})
export class ProductPageComponent implements OnInit {
    private id: number;
    product: Product;
    private subscription: Subscription;

    constructor(private productService: ProductService, private activateRoute: ActivatedRoute) {
        this.subscription = activateRoute.params.subscribe(params => this.id = params['id']);
    }

    ngOnInit() {
        this.productService.getProduct(this.id).then(product => this.product = product);
    }
}