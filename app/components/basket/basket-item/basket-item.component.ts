
import {Component, EventEmitter, Input, Output} from "@angular/core";
import {BasketElement} from "../../../models/basket";
import {BasketService} from "../../../services/basket.service";

@Component({
    moduleId: module.id,
    selector: 'basket-item',
    templateUrl: 'basket-item.component.html',
    styleUrls: ['basket-item.component.css']
})
export class BasketItemComponent {
    @Input() basketElement: BasketElement;

    constructor(private basketService: BasketService) {}

    onRemoveFromCart() {
        this.basketService.removeFromCart(this.basketElement);
    }
}