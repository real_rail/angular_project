import {NgModule} from '@angular/core'
import {BrowserModule} from '@angular/platform-browser'
import {MainComponent} from "./components/main/main.component";
import {FormsModule} from "@angular/forms";
import {ProductFormComponent} from "./components/products/product-form/product-form.component";
import {ProductListComponent} from './components/products/product-list/product-list.component'
import {ProductItemComponent} from "./components/products/product-item/product-item.component";
import {ProductService} from "./services/product.service";
import {HttpModule} from "@angular/http";
import {ProductPageComponent} from "./components/products/product-page/product-page.component";
import {NotFoundComponent} from "./components/not-found/not-found.component";

import {Routes, RouterModule} from '@angular/router';
import {AuthFormComponent} from "./components/authentication/auth-form/auth-form.component";
import {UserService} from "./services/user.service";
import {HeaderPartComponent} from "./components/header-part/header-part.component";
import {RegFormComponent} from "./components/authentication/reg-form/reg-form.component";
import {BasketListComponent} from "./components/basket/basket-list/basket-list.component";
import {BasketItemComponent} from "./components/basket/basket-item/basket-item.component";
import {BasketService} from "./services/basket.service";

const appRoutes: Routes =[
    { path: '', component: ProductListComponent },
    { path: 'auth', component: AuthFormComponent },
    { path: 'reg', component: RegFormComponent },
    { path: 'catalog', component: ProductListComponent},
    { path: 'basket', component: BasketListComponent },
    { path: 'product/:id', component: ProductPageComponent },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(appRoutes)
    ],
    declarations: [
        MainComponent,
        ProductFormComponent,
        ProductListComponent,
        ProductItemComponent,
        ProductPageComponent,
        AuthFormComponent,
        NotFoundComponent,
        HeaderPartComponent,
        RegFormComponent,
        BasketListComponent,
        BasketItemComponent
    ],
    providers: [ProductService, UserService, BasketService],
    bootstrap: [MainComponent]
})
export class AppModule {

}