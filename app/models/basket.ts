export class BasketElement {
    constructor(
        public id: number,
        public id_customer: number,
        public id_product: number,
        public count: number,
        public title: string,
        public price: number,
        public image_url: string,
        public description: string,
    ) {}
}